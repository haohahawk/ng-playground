# # Base image
# FROM node:alpine
# # Create app directory
# RUN mkdir -p /usr/ng-playground
# WORKDIR /usr/ng-playground
# # Install app dependencies
# RUN npm install -g http-server
# # Bundle app source
# COPY ./dist/ng-playground ./
# CMD [ "http-server", "./", "-p", "3000"]

FROM nginx:alpine
COPY ./dist/ng-playground /usr/share/nginx/html
COPY nginx.site.template /etc/nginx/conf.d/
CMD envsubst '${API_HOST}' < /etc/nginx/conf.d/nginx.site.template > /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'
